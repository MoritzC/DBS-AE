import logging


class Log:

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(funcName)s:%(message)s')

    file_handler = logging.FileHandler('D:/datenbanken/backup/log/DBSicherung.log')
    file_handler.setFormatter(formatter)

    logger.addHandler(file_handler)


