from tkinter import *
from model.dump import Dump
import os
from loggingDBS.loggingDBS import Log


class Gui1:

    log = Log()
    dump = Dump()
    _window = Tk()
    _var = StringVar(_window)
    _selection = _var.get()
    _dump_list = os.listdir(dump.path_backup_dumps)
    _dump_list.extend(os.listdir(dump.path_backup_archive))

    def close_window(self):
        """Closes the Tk-root _window."""
        self._window.destroy()
        exit()

    @staticmethod
    def help_window():
        """Opens a new window containing a text describing how to use the application."""
        help_wd = Tk()
        help_wd.title("Datenbanksicherung - Help")
        help_wd.configure(background="Black")
        text = Text(help_wd, bg="black", fg="red", width=100)
        text.insert(INSERT, "This application is able to create and manage backups as dumps of a selected database!\n\n"
                            "- Create dump -\nLets you create a new time-stamped backup of the state the database "
                            "currently has!\n\n- Delete Dump -\nDeletes the backup you selected in the dropdown menu!"
                            "\n\n"
                            "- Open Logfile -\nOpens the logfile containing all important commands, which has been"
                            " excecuted lately!\n\n- Show Help -\nOpens a Window containing this text!\n\n- Exit -\n"
                            "Terminates this application!\n\n\nNote:"
                            "\nWhile the application runs it will auto-generate backups"
                            " twice a day (at 12:00 and 20:00).\n"
                            "When you start the application it will move all backups"
                            " older than 14 days into the 'Archiv'-folder.\nAfter 28 days this folder will only keep "
                            "backups wich are generated at 20:00.")
        text.pack()

    def gui_remove_dump(self):
        """Get's the current selection of the dump_menu and gives it to the method delete_dump().
        The current selection will be deleted out of the dump_menu afterwards."""
        self._selection = self._var.get()
        menu = self.dump_menu["menu"]
        if "..." in self._selection or self._selection == "":
            self.log.logger.error("No dump was selected")
        else:
            archive = "a_"
            if archive in self._selection:
                pathfile = "%s%s" % (self.dump.path_backup_archive, self._selection)
                self.dump.delete_dump(pathfile)
            else:
                pathfile = "%s%s" % (self.dump.path_backup_dumps, self._selection)
                self.dump.delete_dump(pathfile)
            self._var.set("...             ")
            menu.delete(self._selection)

    def gui_add_dump(self):
        """Calls the method create_compressed_dump(). Afterwards dump_list and dump_menu will be updated."""
        menu = self.dump_menu["menu"]
        self.dump.create_compressed_dump()
        self._dump_list = os.listdir(self.dump.path_backup_dumps)
        self._dump_list.extend(os.listdir(self.dump.path_backup_archive))
        self._dump_list.insert(0, "...             ")
        menu.delete(0, "end")
        for string in self._dump_list:
            menu.add_command(label=string,
                             command=lambda value=string: self._var.set(value))

    def __init__(self):
        """The initiator of the main GUI"""
        self._var.set("...             ")
        self.dump_menu = OptionMenu(self._window, self._var, "...             ", *self._dump_list)
        photo = PhotoImage(file="C:/Users/MoritzC/Desktop/bsCorona/AE/gui_label.gif")
        create_dump_bt = Button(self._window, text="Create Dump", width=12, command=(lambda: self.gui_add_dump()))
        delete_dump_bt = Button(self._window, text="Delete Dump", width=12, command=(lambda: self.gui_remove_dump()))
        open_logifle_bt = Button(self._window, text="Open Logfile", width=12,
                                 command=(lambda: os.startfile('D:/datenbanken/backup/log/DBSicherung.log')))
        show_help_bt = Button(self._window, text="Show Help", width=12, command=(lambda: Gui1.help_window()))
        exit_bt = Button(self._window, text="Exit", width=12, command=self.close_window)

        self._window.title("Datenbanksicherung AE")
        self._window.configure(background="black")

        Label(self._window, image=photo, bg="red") .grid(row=0, column=0, sticky=W)

        Label(self._window, text="\n~ Click to see current Dumps ~", bg="black", fg="red", ).grid(row=1, column=0)
        self.dump_menu.grid(row=2, column=0)

        Label(self._window, text="\n~ Delete selected Dump ~", bg="black", fg="red").grid(row=3, column=0)
        delete_dump_bt.grid(row=5, column=0)

        Label(self._window, text="\n~ Create new Dump ~", bg="black", fg="red").grid(row=6, column=0)
        create_dump_bt.grid(row=7, column=0)

        Label(self._window, text="\n~ Open the Logfile ~", bg="black", fg="red").grid(row=8, column=0)
        open_logifle_bt.grid(row=9, column=0)

        Label(self._window, text="\n~ Open Help-Window ~", bg="black", fg="red").grid(row=10, column=0)
        show_help_bt.grid(row=11, column=0)

        Label(self._window, text="\n~ Exit Program ~", bg="black", fg="red") .grid(row=12, column=0)
        exit_bt.grid(row=13, column=0)
        Label(self._window, text="\n", bg="black", fg="white").grid(row=14, column=0)

        self._window.mainloop()



