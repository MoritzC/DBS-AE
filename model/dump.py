from apscheduler.schedulers.background import BackgroundScheduler
import subprocess
import gzip
import os
import time
import datetime
from loggingDBS.loggingDBS import Log


class Dump:

    log = Log()
    _db_name = "sqlteacher"
    _path_backup_archive = "D:/datenbanken/backup/Archiv/"
    _path_backup_dumps = "D:/datenbanken/backup/Dumps/"
    _time_now = time.strftime('%Y%m%d')
    _auto_gen_HH_mm = [21, 12, 1, 12]
    _equals_20_dump = 20

    def create_dump(self):
        """Creates a suc_backup.txt-Dump in D:/datenbanken/backup/Dumps/
        via mysql using windows-cmd with the subprocess-library."""
        try:
            p = subprocess.Popen(
                "D:/datenbanken/mysqldump.exe "
                "-u root --databases %s --result-file=%suc_backup.txt" %
                (self._db_name, self._path_backup_dumps))
            p.communicate()
        except OSError:
            self.log.logger.error('OSError while creating a file')

    def compress_dump(self):
        """Creates a gzip-file(_dump_name.gz) in D:/datenbanken/backup/Dumps/ and copys the content of a suc_backup.txt
        when available. The suc_backup.txt will be deleted afterwards"""
        dump_name = "%s_db_%s_%s.gz" % (self._db_name, time.strftime("%Y%m%d"), time.strftime("%H%M%S"))
        try:
            with open("%suc_backup.txt" % self._path_backup_dumps, "rt") as file_in:
                with gzip.open("%s%s" % (self._path_backup_dumps, dump_name), "wt") as file_out:
                    file_out.writelines(file_in)
            os.remove("%suc_backup.txt" % self._path_backup_dumps)
        except OSError:
            self.log.logger.error('OSError while compressing a file')

    def create_compressed_dump(self, mssg=''):
        """Calls the methods create_dump() and compress_dump(),
        so a dump_name.gz is left in D:/datenbanken/backup/Dumps/.
        parameter: msg(string, optional)"""
        dump_name = "%s_db_%s_%s.gz" % (self._db_name, time.strftime("%Y%m%d"), time.strftime("%H%M%S"))
        self.create_dump()
        self.compress_dump()
        self.log.logger.info(' %s created %s' % (dump_name, mssg))

    def delete_dump(self, dump, mssg=''):
        """Deletes either a dump out of D:/datenbanken/backup/Dumps/ or D:/datenbanken/backup/Archiv/.
        parameter: msg(string, optional), dump(string, is a path to an available file)"""
        archive = "a_"
        try:
            os.remove("%s" % dump)
            self.log.logger.info(' %s deleted %s' % (dump, mssg))
        except OSError:
            self.log.logger.error('OSError while deleting a file')

    def create_dump_twice_day(self):
        """Using the APSscheduler-library. Creates a backgroundscheduler and starts it with its own thread.
        The scheduler calls the method create_compressed_dump() on two different times every day when the process is
        running. The Times are set by var: _auto_gen_HH_mm"""
        scheduler = BackgroundScheduler()
        scheduler.add_job(lambda: self.create_compressed_dump(mssg='(auto-generated)'), 'cron',
                          month='%s-%s' % (self._auto_gen_HH_mm[2], self._auto_gen_HH_mm[3]),
                          day_of_week='mon-fri', hour='%s,%s' % (self._auto_gen_HH_mm[0], self._auto_gen_HH_mm[1]))
        try:
            scheduler.start()
        except (KeyboardInterrupt, SystemExit):
            self.log.logger.error('scheduler was terminated')

    def move_dumps_after_14days(self):
        """Checks if a file in D:/datenbanken/backup/Dumps/ is older than 13 days and if so, it moves the file to
        D:/datenbanken/backup/Archiv/. An "a_" will be added to the name of the file."""
        time_minus14 = datetime.datetime.strptime(self._time_now, '%Y%m%d') - datetime.timedelta(days=14)
        filenames = os.listdir(self.path_backup_dumps)
        before_14d = time_minus14.strftime('%Y%m%d')
        for f in filenames:
            pathfile = '%s%s' % (self.path_backup_dumps, f)
            dump_date = int(pathfile[42:50])
            if dump_date <= int(before_14d):
                try:
                    os.replace(pathfile, '%sa_%s' % (self.path_backup_archive, f))
                    self.log.logger.info(' Auto-moved %s to %s after 14 days' % (f, self.path_backup_archive))
                except OSError:
                    self.log.logger.error('OSError while replace a file')

    def keep_dump20_after_28days(self):
        """Checks if a file in D:/datenbanken/backup/Archiv/ is older than 27 and was created on another time than
        20 o'clock. If so, the file will be deleted."""
        time_minus28 = datetime.datetime.strptime(self._time_now, '%Y%m%d') - datetime.timedelta(days=28)
        filenames = os.listdir(self.path_backup_archive)
        before_28d = time_minus28.strftime('%Y%m%d')
        for f in filenames:
            pathfile = '%s%s' % (self.path_backup_archive, f)
            dump_date = int(pathfile[45:53])
            dump_time = int(pathfile[54:56])
            if dump_date <= int(before_28d) and dump_time != self._equals_20_dump:
                    self.delete_dump(pathfile, mssg='(auto-deleted after 28 days)')

    def get_path_backup_archive(self):
        return self._path_backup_archive

    path_backup_archive = property(get_path_backup_archive)

    def get_path_backup_dumps(self):
        return self._path_backup_dumps

    path_backup_dumps = property(get_path_backup_dumps)










