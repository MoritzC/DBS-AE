import unittest
import os
import time
import datetime
from loggingDBS.loggingDBS import Log
from model.dump import Dump


class TestDump(unittest.TestCase):

    dump = Dump()
    log = Log()

    _time_now = time.strftime('%Y%m%d')

    def create_testfile_dumps(self, month, day, hour, mssg=''):
        filename = 'sqlteacher_db_2020%s%s_%s0000.ut' % (month, day, hour)
        file = open("%s%s" % (self.dump.path_backup_dumps, filename), "w+")
        file.close()
        self.log.logger.debug(mssg)

    def create_testfile_archive(self, month, day, hour, mssg=''):
        filename = 'a_sqlteacher_db_2020%s%s_%s0000.ut' % (month, day, hour)
        file = open("%s%s" % (self.dump.path_backup_archive, filename), "w+")
        file.close()
        self.log.logger.debug(mssg)

    def test_create_compressed_dump(self):
        dump_list = os.listdir(self.dump.path_backup_dumps)
        before = len(dump_list)
        self.dump.create_compressed_dump('(Test-File)')
        dump_list = os.listdir(self.dump.path_backup_dumps)
        after = len(dump_list)
        x = False
        if after == before+1:
            x = True
        self.assertTrue(x)
        self.dump.delete_dump('%s%s' % (self.dump.path_backup_dumps, dump_list[len(dump_list)-1]), '(Test-file)')

    def test_move_dumps_after_14days(self):
        self.create_testfile_dumps('04', '01', '00', 'created a Test-file')
        dump_list_d = os.listdir(self.dump.path_backup_dumps)
        dump_list_a = os.listdir(self.dump.path_backup_archive)
        before_d = len(dump_list_d)
        before_a = len(dump_list_a)
        self.dump.move_dumps_after_14days()
        dump_list_d = os.listdir(self.dump.path_backup_dumps)
        dump_list_a = os.listdir(self.dump.path_backup_archive)
        after_d = len(dump_list_d)
        after_a = len(dump_list_a)
        x = False
        if after_d == before_d-1 and after_a == before_a+1:
            x = True
        self.dump.delete_dump('%s%s' %
                              (self.dump.path_backup_archive, dump_list_a[len(dump_list_a) - 1]), '(Test-file)')
        self.assertTrue(x)

    def test_keep_dump20_after_28days(self):
        time_minus28 = datetime.datetime.strptime(self._time_now, '%Y%m%d') - datetime.timedelta(days=28)
        before_28d = time_minus28.strftime('%Y%m%d')
        self.create_testfile_archive('04', '01', '20', 'created a Test-file')
        self.create_testfile_archive('04', '01', '13', 'created a Test-file')
        x = True
        self.dump.keep_dump20_after_28days()
        dump_list = os.listdir(self.dump.path_backup_archive)
        for f in dump_list:
            pathfile = '%s%s' % (self.dump.path_backup_archive, f)
            dump_date = int(pathfile[45:53])
            dump_time = int(pathfile[54:56])
            if dump_time != 20 and dump_date <= int(before_28d):
                x = False
                break
        self.dump.delete_dump('%s%s' %
                              (self.dump.path_backup_archive, dump_list[len(dump_list) - 1]), 'deleted Test-file')
        self.assertTrue(x)


if __name__ == '__main__':
    unittest.main()
